import test from 'tape';
import moment from 'moment';

import scientificDiff from '../';

test('same year, same month, middle or year, short period', t => {
  const result = scientificDiff('02/06/1983', '22/06/1983');
  const expected = moment([1983, 5, 22]).diff(moment([1983, 5, 2]), 'days') - 1;
  t.equal(result, expected);
  t.end();
});

test('same year, same month, middle or year, long period', t => {
  const result = scientificDiff('04/07/1984', '25/12/1984');
  const expected = moment([1984, 11, 25]).diff(moment([1984, 6, 4]), 'days') - 1;
  t.equal(result, expected);
  t.end();
});

test('different years, different months', t => {
  const result = scientificDiff('03/01/1989', '03/08/1983');
  const expected = moment([1989, 0, 3]).diff(moment([1983, 7, 3]), 'days') - 1;
  t.equal(result, expected);
  t.end();
});

test('different years, different months, before leap day', t => {
  const result = scientificDiff('31/07/1983', '20/02/2016');
  const expected = moment([2016, 1, 20]).diff(moment([1983, 6, 31]), 'days') - 1;
  t.equal(result, expected);
  t.end();
});
