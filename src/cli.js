#!/usr/bin/env node
import path from 'path';

import scientificDiff from './';

const from = process.argv[2];
const to = process.argv[3];
const result = scientificDiff(from, to);

if (result !== undefined) {
  console.log(result);
} else {
  console.log('usage: ' + path.basename(process.argv[1]) + ' DD/MM/YYYY DD/MM/YYYY');
  process.exit(1);
}
