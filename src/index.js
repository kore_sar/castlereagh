import stampit from 'stampit';

function isLeapYear(year) {
  return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
}

function isDayEarlierThanLeapDay({day, month}) {
  return ((month - 1) * 31 + day) <= 31 + 28;
}

const dayOfYearPerMonth = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
const monthMaxDays = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
function isCorrentDayOfMonth({day, month, year}) {
  if (day > monthMaxDays[month - 1]) return false;
  if (month === 2 && day === 29 && !isLeapYear(year)) return false;
  return true;
}

const NumDate = stampit
  .static({
    format: 'MM/DD/YYYY',

    parse(date) {
      const match = /^(0[1-9]|[12]\d|3[01])[\/](0[1-9]|1[0-2])[\/](19\d{2}|2\d{3})$/.exec(date);
      if (!match) return false;

      const result = {day: +match[1], month: +match[2], year: +match[3]};
      return isCorrentDayOfMonth(result) && NumDate(result);
    }
  })
  .methods({
    toDays() {
      let additionalLeapDays = Math.floor(this.year / 4)
        - Math.floor(this.year / 100)
        + Math.floor(this.year / 400);
      if (isLeapYear(this.year) && isDayEarlierThanLeapDay(this)) {
        additionalLeapDays--;
      }

      return (this.year - 1900) * 365
        + dayOfYearPerMonth[this.month - 1]
        + this.day
        + additionalLeapDays;
    },

    scientificDaysDiff(numDate) {
      const from = numDate.toDays();
      const to = this.toDays();
      return Math.abs(to - from) - 1;
    }
  });

export default function scientificDaysDiff(dateFrom, dateTo) {
  const from = NumDate.parse(dateFrom);
  const to = NumDate.parse(dateTo);
  if (from && to) return to.scientificDaysDiff(from);
}
