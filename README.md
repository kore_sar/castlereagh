# scientific-diff
This module shows the number of days between scientific experiments.

# Usage
## CLI
```sh
$ npm i scientific-diff -g
$ scientific-diff 04/07/1984 25/12/1984
```

## Programmatically
```sh
$ npm i scientific-diff
```

```js
import scientificDiff from 'scientific-diff';
const result = scientificDiff('04/07/1984', '25/12/1984');
if (result !== undefined) console.log(result);
```

## Known limitations
* Dates between 01/01/1900 and 31/12/2999 are supported only.
* Only the Gregorian is supported. Doesn't work for Julian and other calendars. E.g. in early 19xx years the Julian was still used in Bulgaria, Ottoman Empire, Russia, Greece etc. More info on [Wikipedia](https://en.wikipedia.org/wiki/Adoption_of_the_Gregorian_calendar).
* Doesn't work properly for dates recorded in Samoa for ranges which include 30 December 2011.
* Also this software assumes that Gregorian would still be the calendar of choice for both dates.
* Could be something else. `¯\_(ツ)_/¯`
